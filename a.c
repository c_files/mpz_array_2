/*

http://scicomp.stackexchange.com/questions/3581/large-array-in-gmp



gcc a.c -lgmp


*/
#include <stdlib.h> // malloc
#include <stdio.h>
#include <gmp.h>



int main ()
{       
      /* definition */
    mpz_t *A;

    /* initialization of A */
    A = (mpz_t *) malloc(100000 * sizeof(mpz_t));
   if (A==NULL) {
          printf("ERROR: Out of memory\n");
          return 1;
                 }

     mpz_set_ui(A+4, 1212121);
     // check
    //gmp_printf ("%Zd\n",A[4]); // 

    /* no longer need A */
   free(A);
        
        return 0;
}
